using OpenQA.Selenium;
using NUnit.Framework;
using OpenQA.Selenium.Chrome;

namespace SimpleAPI.Tests
{
    public class selenium
    {
         public class Selenium						
    {
        IWebDriver driver;

        [SetUp]
		public void startBrowser()
        {
            driver = new ChromeDriver("/home/prashanth/Desktop/chromedriver.exe");
        }

        [Test]
		public void test()
        {
            driver.Url = "http://www.google.co.in";
        }

        [TearDown]
		public void closeBrowser()
        {
            driver.Close();
        }
    }
        
    }
}